module.exports = {
  devServer: {
    open: false // 自动打开浏览器
  },

  productionSourceMap: false,
  lintOnSave: false
};
