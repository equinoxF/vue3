module.exports = {
  root: true,
  env: {
    node: true
  },
  'extends': [
    'plugin:vue/vue3-essential',
    'eslint:recommended',
    '@vue/typescript/recommended'
  ],
  parserOptions: {
    ecmaVersion: 2020
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',

    /**
     *  // "off" or 0 - 关闭规则
        // "warn" or 1 - 将规则视为一个警告（不会影响退出码）
        // "error" or 2 - 将规则视为一个错误 (退出码为1)
     */
    "indent": [2, 2],
    "linebreak-style": [0, "windows"],
    "quotes": [2, "single"],
    "semi": [2, "never"],
    "no-console": 0,
    "no-undef": 0,
    "space-before-function-paren": [2, {
      "anonymous": "always",
      "named": "never"
    }],
    "array-bracket-spacing": 2,
    "block-spacing": [2, "always"],
    "camelcase": 2,
    "comma-spacing": 2,
    "computed-property-spacing": 2,
    "comma-style": [2, "last"],
    "consistent-this": 0,
    "key-spacing": 2,
    "keyword-spacing": 2,
    "newline-after-var": 0,
    "no-array-constructor": 2,
    "no-whitespace-before-property": 2,
    "space-infix-ops": 2,
    "space-unary-ops": 2,
    "arrow-spacing": 2,
    "generator-star-spacing": [2, {
      "before": true,
      "after": false
    }],
    "no-var": 2,
    "no-trailing-spaces": 2,
    "space-before-blocks": 2,
    "eol-last": 2,
    "no-debugger": 0, //禁止使用debugger
  }
}