import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

const app = createApp(App)

// vue3版本的全局函数
// === Vue.prototype.name = 'vue2'
app.config.globalProperties.name = 'vue3 and ts test'
app
  .use(store)
  .use(router)
  .mount('#app')
